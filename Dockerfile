FROM docker:latest

RUN apk add --no-cache go==1.13.14-r0 docker-compose curl bash musl-dev openssl-dev gcc libc-dev make git openssh && \
    rm -rf /var/cache/apk/*
